---
layout: handbook-page-toc
title: "Hotjar"
description: "Website heatmaps and behavior analytics tool"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Uses

Hotjar is utilized for website heatmaps and heavior analytics. The inbound marketing and website team is primarily responsible for its use and setup on our marketing website (about.gitlab.com).

## Support

1. [Status](https://status.hotjar.com/)
1. [Documentation](https://help.hotjar.com/hc/en-us)
